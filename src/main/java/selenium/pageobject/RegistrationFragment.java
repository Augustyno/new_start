package selenium.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import selenium.driver.Driver;

public class RegistrationFragment extends Driver {

    public RegistrationFragment(WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"center_column\"]/p[2]/a[1]")
    private WebElement proceedToCheckoutSummaryButton;

    @FindBy(xpath = "//*[@id=\"email_create\"]")
    private WebElement emailField;

    @FindBy(xpath = "//*[@id=\"SubmitCreate\"]")
    private WebElement submitCreateButton;


    public void stepsBeforeRegistration() {
        proceedToCheckoutSummaryButton.click();
        emailField.click();
        emailField.sendKeys("puzewuta-0292@yopmail.com");
        submitCreateButton.click();
    }
}
