package selenium.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import selenium.driver.Driver;

public class RegistrationNewUser extends Driver {

    public RegistrationNewUser(WebDriver driver){
        super(driver);
    }
//Step II. SIGN IN
// Title
    @FindBy(xpath = "[@id=\"uniform-id_gender2\"]")
    private WebElement checkUniformGenderNewUser;

    @FindBy(xpath = "//*[@id=\"customer_firstname\"]")
    private WebElement firstNameNewUser;

    @FindBy(xpath = "//*[@id=\"customer_lastname\"]")
    private WebElement lastNameNewUser;

    @FindBy(xpath = "//*[@id=\"email\"]")
    private WebElement emailNewUser;

    @FindBy(xpath = "//*[@id=\"passwd\"]")
    private WebElement passwordNewUser;

//Adress

//    @FindBy(xpath = "//*[@id=\"firstname\"]")
//    private WebElement firstNameNewUserAddress;
//
//    @FindBy(xpath = "//*[@id=\"lastname\"]")
//    private WebElement lastNameNewUserAddress;

    @FindBy(xpath = "//*[@id=\"address2\"]")
    private WebElement addressNewUserAddress;

    @FindBy(xpath = "//*[@id=' + Alaska +']")
    private WebElement cityNewUserAddress;

    @FindBy(xpath = "//*[@id=\"uniform-id_state\"]")
    private WebElement stateNewUserAddress;

    @FindBy(xpath = "//*[@id=\"postcode\"]")
    private WebElement zipPostalCodeAddress;

    //@FindBy(xpath = "//*[@id='+ United States +']")
    // private WebElement countryNewUserAddress;

    @FindBy(xpath = "//*[@id=\"phone_mobile\"]")
    private WebElement mobilePhoneNewUser;

    @FindBy (xpath = "//*[@id=\"submitAccount\"]")
    private WebElement submitAccountRegisterButton;

    @FindBy(xpath = "//*[@id=\"alias\"]")
    private WebElement assignAnAddressAliasForFutureReference;

    //Step III. ADDRESS

    @FindBy(xpath = "//*[@id=\"center_column\"]/form/p/button")
    private WebElement submitProceedToCheckoutAddress;

    //Step IV. SHIPPING

    @FindBy(xpath = "//*[@id=\"cgv\"]")
    private WebElement regulations;

    @FindBy(xpath = "//*[@id=\"form\"]/p/button")
    private WebElement submitProceedToCheckoutShipping;

    //Step V.PAYMENT

    @FindBy(xpath = "//*[@id=\"HOOK_PAYMENT\"]/div[1]/div")
    private WebElement payByBankWire;

    @FindBy(xpath = "//*[@id=\"cart_navigation\"]/button")
    private WebElement confirmOrderButton;


public void signInRegistration (){
    checkUniformGenderNewUser.click();
    firstNameNewUser.sendKeys("Anna");
    lastNameNewUser.sendKeys("Kowalska");
    emailNewUser.sendKeys("puzewuta-0292@yopmail.com");
    passwordNewUser.sendKeys("Qwerty123//");
    //firstNameNewUserAddress.sendKeys("Anna");
    //lastNameNewUserAddress.sendKeys("Kowalska");
    addressNewUserAddress.sendKeys("Glen Hwy 168");
    cityNewUserAddress.sendKeys("Sitka");
    stateNewUserAddress.click();
    zipPostalCodeAddress.sendKeys("99801");
    //countryNewUserAddress.click();
    mobilePhoneNewUser.sendKeys("123123123");
    assignAnAddressAliasForFutureReference.clear();
    assignAnAddressAliasForFutureReference.sendKeys("Kowal");

    submitAccountRegisterButton.click();

    submitProceedToCheckoutAddress.click();

    regulations.click();
    submitProceedToCheckoutShipping.click();

    payByBankWire.click();
    confirmOrderButton.click();




}
}