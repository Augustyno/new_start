package selenium.pageobject;
import org.openqa.selenium.WebDriver;
import selenium.driver.Driver;

public class HomePage extends Driver {

    public HomePage(WebDriver driver){
        super (driver);
    }

    WebDriver driver;
    public void navigateToHomePageShop() {

        driver.navigate().to("http://automationpractice.com/index.php");
    }

    public void Quit() {
        //driver.quit();
    }
}
