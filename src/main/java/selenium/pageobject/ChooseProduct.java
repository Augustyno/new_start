package selenium.pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import selenium.driver.Driver;

public class ChooseProduct extends Driver {

    public ChooseProduct (WebDriver driver){
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/a")
            private WebElement womanSection;
    @FindBy(xpath = "//*[@id=\"block_top_menu\"]/ul/li[1]/ul/li[1]/a")
            private WebElement shirts;
    @FindBy(xpath ="//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]" )
            private WebElement shirtsSleeve;
    @FindBy(xpath = "//*[@id=\"center_column\"]/ul/li/div/div[2]/div[2]/a[1]")
            private WebElement addToCart;
    @FindBy(xpath ="//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a" )
            private WebElement proceedToCheckoutButton;


    public void checkProductToBuy() {
        womanSection.click();
        shirts.click();
        shirtsSleeve.click();
        addToCart.click();
        proceedToCheckoutButton.click();
    }
}
